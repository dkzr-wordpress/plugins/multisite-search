<?php
/**
 * Plugin Name: Multisite Search
 * Version: 1.0.1
 * Description: Replace default WordPress site search with a multisite search.
 * Author: Joost de Keijzer
 * Author URI: https://dkzr.nl
 * Plugin URI: https://dkzr.nl
 * Update URI: https://api.dkzr.nl/wp/update-check/
 * Composer Package Name: dkzr/multisite-search
 * Network: true
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **/

if ( ! defined( 'ABSPATH' ) ) { exit; /* Exit if accessed directly */ }

class dkzrMultisiteSearch {
  public function __construct() {
    if ( ! is_admin() && get_network()->site_id == get_current_blog_id() ) {
      add_filter( 'posts_pre_query', [ $this, 'posts_pre_query' ], 10, 2 );
      add_action( 'the_post', [ $this, 'the_post' ], 1, 2 );
      add_action( 'loop_end', [ $this, 'loop_end' ], 1, 1 );
    }
  }

  /**
   * Short-cirquit get_posts when `is_search()`.
   *
   * Set the `found_posts` and `max_num_pages` properties of the WP_Query object
   * so wp_query (passed by reference) can finish get_posts elegantly
   */
  public function posts_pre_query( $posts, $wp_query ) {
    if ( $wp_query->is_search() && empty( $wp_query->query_vars['search-current-site'] ) && empty( $wp_query->query_vars['wc_query'] ) ) {
      $posts = [];
      foreach ( get_sites( [ 'public' => 1, 'deleted' => 0 ] ) as $site ) {
        switch_to_blog( $site->blog_id );
        $q = new WP_Query( array_merge( $wp_query->query, [ 'search-current-site' => true, 'posts_per_page' => -1 ] ) );
        $posts = array_merge(
          $posts,
          array_map( [$this, 'addBlogIdToPost' ], $q->get_posts() )
        );
        restore_current_blog();
      }

      $wp_query->found_posts = count( $posts );
      $wp_query->max_num_pages = ceil( $wp_query->found_posts / ( isset( $wp_query->query_vars['posts_per_page'] ) ? $wp_query->query_vars['posts_per_page'] : get_option( 'posts_per_page' ) ) );
    }

    return $posts;
  }

  public function addBlogIdToPost( $post ) {
    if ( $post && isset( $post->ID ) ) {
      $post->blog_id = get_current_blog_id();
    }
    return $post;
  }

  public function the_post( $post, $wp_query ) {
    if ( $post && isset( $post->blog_id ) && get_current_blog_id() != $post->blog_id ) {
      if ( ms_is_switched() ) {
        restore_current_blog();
        unset( $wp_query->query_vars['ms_blog_id'] );
      }
      if ( get_current_blog_id() != $post->blog_id ) {
        switch_to_blog( $post->blog_id );
        $wp_query->query_vars['ms_blog_id'] = $post->blog_id;
      }

      setup_postdata( $post );
    }
  }

  public function loop_end( $wp_query ) {
    if ( ms_is_switched() && isset( $wp_query->query_vars['ms_blog_id'] ) ) {
      restore_current_blog();
      unset( $wp_query->query_vars['ms_blog_id'] );
    }
  }
}
$dkzrMultisiteSearch = new dkzrMultisiteSearch();
